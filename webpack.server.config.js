const proxyConfigs = require('./webpack.proxy.config');

module.exports = {
  port: 8080,
  host: 'localhost',
  historyApiFallback: true,
  proxy: proxyConfigs,
  hot: true,
};
