import options from './options';

const coefficient = options.multiplier; // just to ensure that module is replaced runtime

/**
 * Description
 *
 * @public
 * @param {number} a - first arg
 * @param {number} b - second arg
 * @return {number} - result
 */
function calculator(a, b) {
  return coefficient * (a + b);
}

export default calculator;
