/**
 * @private
 */
function hello(name, cb) {
  cb(`hello ${name}`);
}

/**
 * @protected
 */
describe('Sinon test', () => {
  /**
   * @protected
   */
  it('should call callback with correct greeting', () => {
    const cb = sinon.spy();

    hello('foo', cb);

    cb.should.have.been.calledWith('hello foo');
  });
});
