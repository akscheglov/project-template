const injector = require('inject!../calculator');

const mocks = {
  multiplier: 4,
};

const calculator = injector({
  './options': mocks,
}).default;

/**
 * @protected
 */
describe('Calculator', () => {
  /**
   * @protected
   */
  it('Should return 20', () => {
    const result = calculator(3, 2);
    result.should.equal(20);
  });
});
