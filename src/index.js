import './styles/index.styl';
import calculator from './example/calculate';

const result = document.createElement('div');
const text = document.createTextNode(calculator(2, 3));
result.appendChild(text);

document.getElementById('root').appendChild(result);
