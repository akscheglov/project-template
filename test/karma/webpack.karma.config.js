const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {

  devtool: 'inline-source-map',

  resolveLoader: {
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js'],
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
      },
    ],

    postLoaders: [
      {
        test: /\.js$/,
        exclude: [
          /node_modules/,
          /\.webpack.js$/,
          /\.spec\.js$/,
        ],
        loader: 'istanbul-instrumenter',
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(['coverage'], {
      root: process.cwd(),
      verbose: true,
      dry: false,
    }),
  ],
};
