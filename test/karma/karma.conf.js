const wpConfig = require('./webpack.karma.config');

const coverageDir = '../../coverage/'; // tbd: clean this directory (do not use webpack plugin)

/**
 * @private
 */
module.exports = function setup(config) {
  config.set({
    browsers: ['PhantomJS'],

    singleRun: false,

    autoWatch: true,

    frameworks: [
      'mocha',
      'chai',
      'sinon',
      'sinon-chai',
    ],

    files: [
      'karma.tests.entry.js',
    ],

    preprocessors: {
      'karma.tests.entry.js': [
        'webpack',
        'sourcemap',
      ],
      'src/**/*.js': [
        'coverage',
      ],
    },

    reporters: [
      'mocha',
      'coverage',
    ],

    webpack: wpConfig,

    webpackServer: {
      noInfo: true,
    },

    coverageReporter: {
      type: 'html',
      dir: coverageDir,
    },
  });
};
