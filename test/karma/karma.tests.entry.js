/**
 * This is need to find and build all tests files using webpack.
 */
const context = require.context('../../src', true, /.+\.spec\.js$/);
context.keys().forEach(context);
