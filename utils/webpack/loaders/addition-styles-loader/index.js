/* eslint-disable strict */
'use strict';
/* eslint-enable strict */

const fs = require('fs');
const path = require('path');

/**
 * @param {string} filePath
 * @return {boolean}
 */
function isFileExists(filePath) {
  try {
    return fs.statSync(filePath).isFile();
  } catch (err) {
    return false;
  }
}

/**
 * Add to dependence the related file from 'root' directory specified in loader config.
 * This loader used for adding stylus stiles to source.
 * For example, you should add some addition styles during build based on environment settings.
 *
 * Usage:
 *   1. Specify in webpack config the absolute path to root folder:
 *     <pre>
 *         <code>
 *            {
 *              ...
 *              additionStyles: {
 *                root: path.join(rootDir, 'addition-styles', process.env.MODE),
 *              },
 *            }
 *         </code>
 *     </pre>
 *   2. Add loader to the chain
 *     <pre>
 *         <code>
 *            {
 *              test: /\.styl$/,
 *              loader: ExtractTextPlugin.extract('css!postcss!stylus?resolve url!addition-styles'),
 *            },
 *         </code>
 *     </pre>
 *
 *  Now when webpack will build file path.resolve(webpack.context, 'relative_path_to_styles.styl'),
 *  loader will append "@require path.join(params.root, 'relative_path_to_styles.styl')" to
 *  the end of original file if addition file exists.
 */
module.exports = function load(content) {
  if (this.cacheable) this.cacheable();
  const callback = this.async();

  // There is a webpack bug https://github.com/webpack/webpack/issues/1289 that aliased loader have empty query
  const params = this.options.additionStyles;
  if (!params.root) throw new Error('Please, specify root folder for the loader');
  if (!path.isAbsolute(params.root)) throw Error('Root path should be absolute');

  const relative = path.relative(this.options.context, this.resource);
  const relatedResourcePath = path.join(params.root, relative);

  let transformed = content;
  if (isFileExists(relatedResourcePath)) {
    this.addDependency(relatedResourcePath);
    transformed = `${content}\n@require '${relatedResourcePath}'\n`;
  }

  callback(null, transformed);
};
