const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const postcssMergeRules = require('postcss-merge-rules');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const serverConfigs = require('./webpack.server.config');

const isDevelopment = process.env.NODE_ENV !== 'production';

const rootDir = __dirname;
const srcDir = 'src';
const buildDir = 'public';
const modulesDir = 'node_modules';
const assetsDir = 'assets';
const srcPath = path.join(rootDir, srcDir);
const buildPath = path.join(rootDir, buildDir);

const sourceMap = 'source-map';

function addHash(template, hash) {
  return !isDevelopment ?
    template.replace(/\.[^.]+$/, `.[${hash}]$&`) :
    `${template}?hash=[${hash}]`;
}

module.exports = {

  context: srcPath,

  entry: {
    index: './index',
  },

  output: {
    path: path.join(buildPath, assetsDir),
    filename: addHash('[name].js', 'hash'),
  },

  devtool: sourceMap,

  resolve: {
    modulesDirectories: [modulesDir],
    extensions: ['', '.js', '.styl'],
    root: srcPath,
  },

  resolveLoader: {
    modulesDirectories: [modulesDir],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js'],
    alias: {
      'addition-styl': path.join(rootDir, 'utils', 'webpack', 'loaders', 'addition-styles-loader'),
    },
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: srcPath,
      },
      {
        test: /\.js$/,
        loader: 'eslint',
        include: srcPath,
      },
      {
        test: /\.styl$/,
        loader: ExtractTextPlugin.extract(
          'style',
          'css?!postcss!stylus?resolve url!addition-styl'
        ),
      },
      {
        test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
        loader: addHash('url?name=[path][name].[ext]&limit=10000', 'hash:6'),
      },
    ],
  },

  additionStyles: {
    root: path.join(rootDir, 'src', 'example'),
  },

  eslint: {
    emitError: true,
  },

  postcss: () => [
    autoprefixer({
      browsers: ['last 2 versions'],
    }),
    postcssMergeRules, // TODO: check not working for merged files
  ],

  plugins: [
    new CleanWebpackPlugin([buildPath], {
      root: rootDir,
      verbose: true,
      dry: false,
    }),
    new ExtractTextPlugin(
      addHash('[name].css', 'contenthash'), {
        allChunks: true,
        disable: isDevelopment,
      }),
    new webpack.DefinePlugin({
      isDevelopment: JSON.stringify(isDevelopment),
    }),
    new HtmlWebpackPlugin({
      template: path.join(srcPath, 'index.html'),
      filename: '../index.html',
    }),
    new webpack.BannerPlugin('Copyright For Template Project.'), // tbd: move text to file
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: true,
        drop_console: true,
      },
    }),
  ],

  devServer: serverConfigs,
};
