# Project Template

This is a basic template for a [Webpack](http://webpack.github.io/) project

## Install

#### GIT

```sh
git clone git@bitbucket.org:akscheglov/project-template.git
```

Then, install npm dependencies.

```sh
npm i
```


## Build

```sh
npm run build
```


## Test

```sh
npm run test
```


## Lint

```sh
npm run lint
```


## License

MIT © [Alexander Scheglov](https://bitbucket.org/akscheglov/)
